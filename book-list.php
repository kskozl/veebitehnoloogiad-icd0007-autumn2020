<?php

require_once 'vendor/tpl.php';
require_once 'Book.php';
require_once 'Author.php';

require_once "database.php";

$conn = getConnection();
$stmt = $conn->prepare("select * from books");
$stmt->execute();
$books = [];
$message = "";

foreach ($stmt as $book) {

    $id = $book["id"];
    $title = $book["title"];
    $author = $book["author"];
    $grade = $book["grade"];
    $isRead = $book["isRead"];

    $name = $conn->prepare("select firstName, lastName from authors where id='$author'");
    $name->execute();

    $authorFullName = "";

    foreach ($name as $n) {
        $authorFirstName = isset($n["firstName"]) ? $n["firstName"] : " ";
        $authorLastName = isset($n["lastName"]) ? $n["lastName"] : " ";
        $authorFullName = $authorFirstName . " " . $authorLastName;
        if($author == 0){
            $authorFullName = "";
        }
    }
    $title = urldecode($title);
    array_push($books, new Book($title, $authorFullName, $grade, $isRead, $id));
}

$index = isset($_GET["index"]) ? $_GET["index"] : "";

if ($index == 1) {
    $message = "Raamat salvestanud";
} else if ($index == 2){
    $message = "Raamat muutunud";
} else if ($index == 3){
    $message = "Raamat kustutatud";
}

$data = [
    'books' => $books,
    'message' => $message,
];
print renderTemplate('book-list.html', $data);



