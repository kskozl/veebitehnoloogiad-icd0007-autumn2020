<?php

require_once 'vendor/tpl.php';
require_once 'Book.php';
require_once 'Author.php';

require_once "database.php";
$conn = getConnection();
$stmt = $conn -> prepare("select * from authors");
$stmt -> execute();

$authors = [];
$message = "";

foreach ($stmt as $author){
    array_push($authors,
        new Author($author["firstName"], $author["lastName"], $author["grade"], $author["id"]));
}

$index = isset($_GET["index"]) ? $_GET["index"] : "";
if ($index == 1) {
    $message = "Author salvestanud";
} else if ($index == 2){
    $message = "Author muutunud";
} else if ($index == 3){
    $message = "Author kustutatud";
}

$data = [
    'authors' => $authors,
    'message' => $message,
];

print renderTemplate('author-list.html', $data);

