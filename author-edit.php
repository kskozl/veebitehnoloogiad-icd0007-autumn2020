<?php
require_once("database.php");
require_once 'vendor/tpl.php';
require_once 'Author.php';


if ($_SERVER["REQUEST_METHOD"] === "GET") {
    $id = $_GET["id"];

    $conn = getConnection();
    $stmt = $conn-> prepare("select firstName, lastName, grade from authors where id ='$id'");
    $stmt -> execute();

    $firstName = "";
    $lastName = "";
    $grade = "";

    foreach ($stmt as $author){
        $firstName = $author["firstName"];
        $lastName = $author["lastName"];
        $grade = $author["grade"];
    }

} else {
    $id = $_POST["id"];
    $firstName = $_POST["firstName"];
    $lastName = $_POST["lastName"];
    $grade = isset($_POST["grade"]) ? ($_POST["grade"]) : 0;
}

$errorMessage = "";
$falseName = "";
$falseLastName = "";
$deleteButton = isset($_POST["deleteButton"]) ? ($_POST["deleteButton"]) : "";
$submitButton = isset($_POST["submitButton"]) ? ($_POST["submitButton"]) : "";


    if ($_SERVER["REQUEST_METHOD"] === "POST" && $deleteButton == "Kustuda"){

        $errorMessage = "";
        $id = $_POST["id"];
        $conn = getConnection();
        $stmt = $conn-> prepare("delete from authors where id ='$id'");
        $stmt -> execute();

        header("Location: /author-list.php?index=3");
        exit();

    }

    else if ($_SERVER["REQUEST_METHOD"] === "POST" and
        (strlen($firstName) < 1 or strlen($firstName) > 21 or strlen($lastName) < 2 or strlen($lastName) > 22)) {

        $id = $_POST["id"];
        $firstName = $_POST["firstName"];
        $lastName = $_POST["lastName"];
        $grade = $_POST["grade"];

        $errorMessage = "ERROR";
        $falseName .= $_POST["firstName"];
        $falseLastName .= $_POST["lastName"];

    }

    else if ($_SERVER["REQUEST_METHOD"] === "POST" && $submitButton == "Salvesta" &&
        (strlen($firstName) > 1 or strlen($firstName) < 21 or strlen($lastName) > 2 or strlen($lastName) < 22)){

        $errorMessage = "";
        $id = $_POST["id"];
        $firstName = $_POST["firstName"];
        $lastName = $_POST["lastName"];
        $grade = $_POST["grade"];


        $conn = getConnection();
        $stmt = $conn-> prepare("update authors set firstName = '$firstName', lastName = '$lastName', grade = '$grade' where id ='$id'");
        $stmt -> execute();

        header("Location: /author-list.php?index=2");
        exit();

    }

if($errorMessage == ""){
    $author = new Author($firstName, $lastName, $grade, $id);
} else {
    $author = new Author($falseName, $falseLastName, $grade, $id);
}

$data = [
    "errorMessage" => $errorMessage,
    "author" => $author,
];

print renderTemplate('author-edit.html', $data);


