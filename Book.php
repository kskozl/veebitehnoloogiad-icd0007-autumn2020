<?php

class Book
{

    public $id;
    public $title;
    public $authorFullName;
    public $grade;
    public $isRead;

    public function __construct($title, $authorFullName, $grade, $isRead, $id = null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->authorFullName = $authorFullName;
        $this->grade = $grade;
        $this->isRead = $isRead;
    }

    function getData(){
        return " ID: ".$this->id." TITLE: ".$this->title." FULLNAME: ".$this->authorFullName;
    }

}
