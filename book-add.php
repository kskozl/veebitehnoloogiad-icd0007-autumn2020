<?php

require_once 'vendor/tpl.php';
require_once "database.php";
require_once 'Book.php';
require_once 'Author.php';

$conn = getConnection();

$title = isset($_POST["title"]) ? urlencode($_POST["title"]) : "";
$authorID = isset($_POST["authorFullName"]) ? $_POST["authorFullName"] : 0;
$grade = isset($_POST["grade"]) ? $_POST["grade"] : 0;
$isRead = isset($_POST["isRead"]) ? "1" : "0";

$falseTitle = "";
$errorMessage = "";

if ($_SERVER["REQUEST_METHOD"] === "POST" and (strlen($title) >= 3 and strlen($title) <= 23)) {

    $errorMessage = "";
    $title = urlencode($title);
    $stmt = $conn->prepare("insert into books (title, grade, isRead, author) values ('$title', '$grade', '$isRead', '$authorID')");
    $stmt->execute();
    header("Location: /book-list.php?index=1");
    exit();

}
if ($_SERVER["REQUEST_METHOD"] === "GET") {
    $errorMessage = "";

} else {
    $errorMessage = "ERROR";
    $falseTitle .= $title;
}

if ($isRead == 1) {
    $isRead = true;
} else {
    $isRead = false;
}

if (strlen($title) != "" and $errorMessage == "ERROR") {
    $book = new Book($falseTitle, $authorID, $grade, $isRead);
} else {
    $book = new Book($title, $authorID, $grade, $isRead);
}

$authors = [];
$conn = getConnection();
$stmt = $conn->prepare("select * from authors");
$stmt->execute();
foreach ($stmt as $author) {
    array_push($authors, new Author($author["firstName"], $author["lastName"], $author["grade"], $author["id"]));
}

function AuthorByID($authorID)
{
    $conn = getConnection();
    $stmt = $conn->prepare("select id, firstName, lastName, grade from authors where id=$authorID");
    $stmt->execute();

    $id = 0;
    $firstName = "";
    $lastName = "";
    $grade = 0;

    foreach ($stmt as $author) {
        $id = isset($author["id"]) ? $author["id"] : 0;
        $firstName = isset($author["firstName"]) ? $author["firstName"] : "";
        $lastName = isset($author["lastName"]) ? $author["lastName"] : "";
        $grade = isset($author["grade"]) ? $author["grade"] : 0;
    }
    return new Author($firstName, $lastName, $grade, $id);
}

$data = [
    "errorMessage" => $errorMessage,
    "book" => $book,
    "authors" => $authors,
    "id" => $authorID,
    "author" => AuthorByID($authorID),
];

print renderTemplate('book-add.html', $data);

