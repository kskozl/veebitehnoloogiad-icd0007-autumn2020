<?php
require_once 'vendor/tpl.php';
require_once "database.php";
require_once 'Book.php';
require_once 'Author.php';

if ($_SERVER["REQUEST_METHOD"] === "GET") {
    $id = $_GET["id"];

    $conn = getConnection();
    $stmt = $conn->prepare("select title, grade, author, isRead from books where id ='$id'");
    $stmt->execute();
    $title = "";
    $authorID = 0;
    $grade = 0;
    $isRead = "";

    foreach ($stmt as $book) {
        $title = isset($book["title"]) ? ($book["title"]) : "";
        $grade = isset($book["grade"]) ? ($book["grade"]) : 0;
        $authorID = isset($book["author"]) ? ($book["author"]) : 0;
        $isRead = isset($book["isRead"]) ? ($book["isRead"]) : "0";
    }
} else {
    $id = isset($_POST["id"]) ? ($_POST["id"]) : 0;
    $title = isset($_POST["title"]) ? ($_POST["title"]) : "";
    $authorID = isset($_POST["author1"]) ? ($_POST["author1"]) : 0;
    $grade = isset($_POST["grade"]) ? ($_POST["grade"]) : 0;
    $isRead = isset($_POST["isRead"]) ? ($_POST["isRead"]) : "0";

    //print_r($_POST);
}
$errorMessage = "";
$falseTitle = "";
$deleteButton = isset($_POST["deleteButton"]) ? ($_POST["deleteButton"]) : "";
$submitButton = isset($_POST["submitButton"]) ? ($_POST["submitButton"]) : "";


if ($_SERVER["REQUEST_METHOD"] === "POST" && $deleteButton == "Kustuda") {

    $errorMessage = "";
    $id = $_POST["id"];
    $conn = getConnection();
    $stmt = $conn->prepare("delete from books where id ='$id'");
    $stmt->execute();
    header("Location: index.php?command=show_book_list&index=3");
    exit();


} else if ($_SERVER["REQUEST_METHOD"] === "POST" and (strlen($title) < 3 or strlen($title) > 23)) {

    $id = $_POST["id"];
    $title = $_POST["title"];
    $authorID = $_POST["author1"];
    $grade = $_POST["grade"];
    $isRead = $_POST["isRead"];

    $errorMessage = "ERROR";
    $falseTitle .= $_POST["title"];


} else if ($_SERVER["REQUEST_METHOD"] === "POST" && $submitButton == "Salvesta") {

    $id = isset($_POST["id"]) ? ($_POST["id"]) : 0;
    $title = isset($_POST["title"]) ? ($_POST["title"]) : "";
    $authorID = isset($_POST["author1"]) ? ($_POST["author1"]) : 0;
    $grade = isset($_POST["grade"]) ? ($_POST["grade"]) : "0";
    $isRead = isset($_POST["isRead"]) ? ($_POST["isRead"]) : "0";
    $errorMessage = "";

    $conn = getConnection();
    $stmt = $conn->prepare("update books set title = '$title', grade = '$grade', author = '$authorID', isRead = '$isRead' where id ='$id'");
    $stmt->execute();

    header("Location: index.php?command=show_book_list&index=2");
    exit();
}

if ($isRead == 1){
    $isRead = true;
} else {
    $isRead = false;
}

if ($errorMessage == "") {
    $book = new Book($title, AuthorByID($authorID) -> getFullName(), $grade, $isRead, $id);
} else {
    $book = new Book($falseTitle, AuthorByID($authorID) -> getFullName(), $grade, $isRead, $id);
}

function AuthorByID($authorID)
{
    $conn = getConnection();
    $stmt = $conn->prepare("select id, firstName, lastName, grade from authors where id=$authorID");
    $stmt->execute();

    $id = 0;
    $firstName = "";
    $lastName = "";
    $grade = 0;

    foreach ($stmt as $author){
        $id = isset($author["id"]) ? $author["id"] : 0;
        $firstName = isset($author["firstName"]) ? $author["firstName"] : "";
        $lastName = isset($author["lastName"]) ? $author["lastName"] : "";
        $grade = isset($author["grade"]) ? $author["grade"] : 0;
    }
    return new Author($firstName, $lastName, $grade, $id);
}

$authors = [];
$conn = getConnection();
$stmt = $conn->prepare("select * from authors");
$stmt->execute();
foreach ($stmt as $author) {
    array_push($authors, new Author($author["firstName"], $author["lastName"], $author["grade"], $author["id"]));
}


$data = [
    "errorMessage" => $errorMessage,
    "book" => $book,
    "authors" => $authors,
    "id" => $authorID,
    "author" => AuthorByID($authorID),
];

print renderTemplate('book-edit.html', $data);



//1) Expecting to find text '8a036a8500 8a036a8501' once but found it 0 times at [C:\Users\kseni\PhpstormProjects\icd0007tests\hw5.php line 25]
//	in canSaveBooksWithSingleAuthor
//canSaveBooksWithSingleAuthor failed

//2) Did not find option with text '407060db20 407060db21' and value attribute at [C:\Users\kseni\PhpstormProjects\icd0007tests\hw5.php line 47]
//	in canUpdateBooksWithSingleAuthor
//canUpdateBooksWithSingleAuthor failed
