<?php

class Author
{

    public $id;
    public $firstName;
    public $lastName;
    public $grade;

    public function __construct($firstName, $lastName, $grade, $id = null)
    {
        $this->id = $id;
        $this->firstName = trim($firstName, " ");
        $this->lastName = trim($lastName, " ");
        $this->grade = $grade;
    }

    public function getFullName()
    {
        $fullName = urldecode($this -> firstName) . " " . urldecode($this -> lastName);
        return trim($fullName, " ");
    }

    function getData(){
        return " ID: ".$this->id." NAME: ".$this.$this->getFullName();
    }

}
