<?php

//включает и вычисляет специфицированный файл в процессе выполнения скрипта.
//sisaldab ja hindab määratud faili skripti käivitamisel.
require_once("database.php");
require_once 'vendor/tpl.php';
require_once 'Author.php';

//устанавливает соединение с сервером
$conn = getConnection();

//$id = $_POST["id"];
$firstName = isset($_POST["firstName"]) ? $_POST["firstName"] : "";
$lastName = isset($_POST["lastName"]) ? $_POST["lastName"] : "";
$grade = isset($_POST["grade"]) ? $_POST["grade"] : 0;

$falseName = "";
$falseLastName = "";
$errorMessage = "";
$isError = false;

if ($_SERVER["REQUEST_METHOD"] === "POST" and strlen($firstName) >= 1 and strlen($firstName) <= 21 and strlen($lastName) >= 2 and strlen($lastName) <= 22) {
    $errorMessage = "";

    $stmt = $conn->prepare("insert into authors (firstName, lastName, grade) values ('$firstName', '$lastName', '$grade')");
    $stmt->execute();
    header("Location: /author-list.php?index=1");
    exit();

} if ($_SERVER["REQUEST_METHOD"] === "POST" and (strlen($firstName) < 1 or strlen($firstName) > 21 or strlen($lastName) < 2 or strlen($lastName) > 22)) {
    $errorMessage = "ERROR";

    $falseName .= $firstName;
    $falseLastName .= $lastName;

    $isError = true;
}

if($errorMessage == ""){
    $author = new Author($firstName, $lastName, $grade);
} else {
    $author = new Author($falseName, $falseLastName, $grade);
}

$data = [
    "errorMessage" => $errorMessage,
    "author" => $author,
];

print renderTemplate('author-add.html', $data);
