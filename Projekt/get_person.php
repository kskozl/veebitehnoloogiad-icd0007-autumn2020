<?php

require_once("Person.php");

$person = new Person("Joht", "Dõe", ["5555555", "34567890"]);
header("Content-Type: application/json");
print json_encode($person, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);