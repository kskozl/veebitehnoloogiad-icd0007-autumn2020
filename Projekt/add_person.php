<?php
require_once("Person.php");

if ($_SERVER["REQUEST_METHOD"] === "POST"){

    $json = file_get_contents("php://input");
    $personData = json_decode($json, true);

    $person = new Person($personData["firstName"], $personData["lastName"], $personData["phoneNumbers"]);

    $errors = $person -> validata();

    if (count($errors) > 0){
        http_response_code(400);
        header("Content-Type: application/json");
        print json_encode(["errors" => $errors]);
    } else {
        header("Content-Type: application/json");
        print_r(json_encode($person));
    }

} else {
    http_response_code(405);
}
