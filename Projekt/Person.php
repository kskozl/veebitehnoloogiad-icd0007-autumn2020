<?php

class Person{
    public $firstName;
    public $lastName;
    public $phoneNumbers;

    public function __construct($firstName, $lastName, $phoneNumbers){
        $this -> firstName = $firstName;
        $this -> lastName = $lastName;
        $this -> phoneNumbers = $phoneNumbers;
    }

    public function validata(){

        $errors = [];

        if(isset($this->firstName)){
            $errors[] = "failed firstName";
        }
        if (isset($this->lastName)){
            $errors[] = "failed lastName";
        }
        if (isset($this->phoneNumbers)){
            $errors[] = "failed phoneNumbers";
        }
        return $errors;
    }

}