<?php
require_once("database.php");
require_once 'vendor/tpl.php';
require_once 'Author.php';
require_once 'Book.php';
require_once 'Request.php';
//require_once 'scripts.php';

$request = new Request($_REQUEST);

$data = [];
$command = $request -> param('command') ? $request -> param('command') : 'show_book_list';


if ($command === 'show_book_form'){
    $conn = getConnection();
    $title = isset($_POST["title"]) ? $_POST["title"] : "";
    $authorID = isset($_POST["authorFullName"]) ? $_POST["authorFullName"] : 0;
    $grade = isset($_POST["grade"]) ? $_POST["grade"] : 0;
    $isRead = isset($_POST["isRead"]) ? "1" : "0";
    $falseTitle = "";
    $errorMessage = "";

    if ($_SERVER["REQUEST_METHOD"] === "POST" and (strlen($title) >= 3 and strlen($title) <= 23)) {
        $errorMessage = "";
        $title = urlencode($title);
        $stmt = $conn->prepare("insert into books (title, grade, isRead, author) values ('$title', '$grade', '$isRead', '$authorID')");
        $stmt->execute();
        header("Location: index.php?command=show_book_list&index=1");
        exit();
    }
    if ($_SERVER["REQUEST_METHOD"] === "GET") {
        $errorMessage = "";
    } else {
        $errorMessage = "ERROR";
        $falseTitle .= $title;
    }
    if ($isRead == 1) {
        $isRead = true;
    } else {
        $isRead = false;
    }
    if (strlen($title) != "" and $errorMessage == "ERROR") {
        $book = new Book($falseTitle, $authorID, $grade, $isRead);
    } else {
        $book = new Book($title, $authorID, $grade, $isRead);
    }
    $authors = [];
    $conn = getConnection();
    $stmt = $conn->prepare("select * from authors");
    $stmt->execute();
    foreach ($stmt as $author) {
        array_push($authors, new Author($author["firstName"], $author["lastName"], $author["grade"], $author["id"]));
    }
    function AuthorByID($authorID)
    {
        $conn = getConnection();
        $stmt = $conn->prepare("select id, firstName, lastName, grade from authors where id=$authorID");
        $stmt->execute();
        $id = 0;
        $firstName = "";
        $lastName = "";
        $grade = 0;
        foreach ($stmt as $author) {
            $id = isset($author["id"]) ? $author["id"] : 0;
            $firstName = isset($author["firstName"]) ? $author["firstName"] : "";
            $lastName = isset($author["lastName"]) ? $author["lastName"] : "";
            $grade = isset($author["grade"]) ? $author["grade"] : 0;
        }
        return new Author($firstName, $lastName, $grade, $id);
    }
    $data = [
        "errorMessage" => $errorMessage,
        "book" => $book,
        "authors" => $authors,
        "id" => $authorID,
        "author" => AuthorByID($authorID),
    ];
    print renderTemplate('book-add.html', $data);
}


if ($command === 'show_book_list'){
    $conn = getConnection();
    $stmt = $conn->prepare("select * from books");
    $stmt->execute();
    $books = [];
    $message = "";
    foreach ($stmt as $book) {
        $id = $book["id"];
        $title = $book["title"];
        $author = $book["author"];
        $grade = $book["grade"];
        $isRead = $book["isRead"];
        $name = $conn->prepare("select firstName, lastName from authors where id='$author'");
        $name->execute();
        $authorFullName = "";
        foreach ($name as $n) {
            $authorFirstName = isset($n["firstName"]) ? $n["firstName"] : " ";
            $authorLastName = isset($n["lastName"]) ? $n["lastName"] : " ";
            $authorFullName = trim($authorFirstName, " ") . " " . trim($authorLastName, " ");
            if($author == 0){
                $authorFullName = "";
            }
        }
//        $authorFullName = trim($authorFullName, " ");
        $title = urldecode($title);
        array_push($books, new Book($title, $authorFullName, $grade, $isRead, $id));
    }
    $index = isset($_GET["index"]) ? $_GET["index"] : "";
    if ($index == 1) {
        $message = "Raamat salvestanud";
    } else if ($index == 2){
        $message = "Raamat muutunud";
    } else if ($index == 3){
        $message = "Raamat kustutatud";
    }
    $data = [
        'books' => $books,
        'message' => $message,
    ];
    print renderTemplate('book-list.html', $data);
}


if ($command === 'show_author_form'){
    $conn = getConnection();
//    $id = $_POST["id"];
    $firstName = isset($_POST["firstName"]) ? $_POST["firstName"] : "";
    $lastName = isset($_POST["lastName"]) ? $_POST["lastName"] : "";
    $grade = isset($_POST["grade"]) ? $_POST["grade"] : 0;
    $falseName = "";
    $falseLastName = "";
    $errorMessage = "";
    $isError = false;

    if ($_SERVER["REQUEST_METHOD"] === "POST" and strlen($firstName) >= 1 and strlen($firstName) <= 21 and strlen($lastName) >= 2 and strlen($lastName) <= 22) {
        $errorMessage = "";

        $stmt = $conn->prepare("insert into authors (firstName, lastName, grade) values ('$firstName', '$lastName', '$grade')");
        $stmt->execute();
        header("Location: index.php?command=show_author_list&index=1");
        exit();

    } if ($_SERVER["REQUEST_METHOD"] === "POST" and (strlen($firstName) < 1 or strlen($firstName) > 21 or strlen($lastName) < 2 or strlen($lastName) > 22)) {
        $errorMessage = "ERROR";
        $falseName .= $firstName;
        $falseLastName .= $lastName;
        $isError = true;
    }
    if($errorMessage == ""){
        $author = new Author($firstName, $lastName, $grade);
    } else {
        $author = new Author($falseName, $falseLastName, $grade);
    }
    $data = [
        "errorMessage" => $errorMessage,
        "author" => $author,
    ];
    print renderTemplate('author-add.html', $data);
}


if ($command === 'show_author_list'){
    $conn = getConnection();
    $stmt = $conn -> prepare("select * from authors");
    $stmt -> execute();
    $authors = [];
    $message = "";
    foreach ($stmt as $author){
        array_push($authors,
            new Author($author["firstName"], $author["lastName"], $author["grade"], $author["id"]));
    }
    $index = isset($_GET["index"]) ? $_GET["index"] : "";
    if ($index == 1) {
        $message = "Author salvestanud";
    } else if ($index == 2){
        $message = "Author muutunud";
    } else if ($index == 3){
        $message = "Author kustutatud";
    }
    $data = [
        'authors' => $authors,
        'message' => $message,
    ];
    print renderTemplate('author-list.html', $data);
}